{
  description = "An online ebook collection browser";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
        artifacts = final: prev: {
          bookshelf = prev.bookshelf;
        };
      in
      rec {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            gopls
            golangci-lint
          ];

          inputsFrom = [ packages.bookshelf ];
        };

        packages = {
          bookshelf = pkgs.buildGoModule {
            pname = "bookshelf";
            version = "0.5.0";

            vendorHash = "sha256-MHqHTa5ZAhTaHRWA+TMAUmFrlf89fwuh1YWvux/wwvI=";

            src = ./.;

            doCheck = pkgs.stdenv.buildPlatform == pkgs.stdenv.hostPlatform;

            meta = with pkgs.lib; {
              homepage = "https://gitlab.com/greizgh/bookshelf";
              description = "Online ebook collection browser";
              license = licenses.gpl3;
            };
          };
        };
        defaultPackage = packages.bookshelf;
        overlay = artifacts;
      }
    );
}
