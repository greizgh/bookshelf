// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"fmt"
	"os"

	"gitlab.com/greizgh/bookshelf/ebook"

	"github.com/jedib0t/go-pretty/table"
	"github.com/spf13/cobra"
)

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List ebooks in directory",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		files, err := ebook.FindEpubs(args[0])
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not find files in %q: %v\n", args[0], err)
			os.Exit(1)
		}

		t := table.NewWriter()
		t.SetOutputMirror(os.Stdout)
		t.AppendHeader(table.Row{"Author", "Title", "Series"})

		loader := &ebook.EpubLoader{}

		for _, file := range files {
			book, _ := loader.Load(file)
			t.AppendRow([]interface{}{book.Author, book.Title, book.Series})
		}

		t.Render()
	},
}
