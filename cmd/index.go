// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"fmt"
	"os"

	"gitlab.com/greizgh/bookshelf/cover"
	"gitlab.com/greizgh/bookshelf/db"
	"gitlab.com/greizgh/bookshelf/ebook"
	"gitlab.com/greizgh/bookshelf/indexer"
	"go.uber.org/zap"

	"github.com/adrg/xdg"
	"github.com/spf13/cobra"
)

var indexCmd = &cobra.Command{
	Use:   "index",
	Short: "Index ebooks in directory",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		files, err := ebook.FindEpubs(args[0])
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not find files in %q: %v\n", args[0], err)
			os.Exit(1)
		}

		database, err := db.GetDB()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not access database: %v\n", err)
			os.Exit(1)
		}
		defer database.Close()

		idx := indexer.New(db.New(database), new(ebook.EpubLoader), cover.New(xdg.DataHome))

		results := idx.Index(files)

		zap.L().Info(
			"Indexation successful",
			zap.Int("success", results[indexer.Success]),
			zap.Int("skipped", results[indexer.Skipped]),
			zap.Int("error", results[indexer.Error]),
		)
	},
}
