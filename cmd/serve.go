// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/greizgh/bookshelf/cover"
	"gitlab.com/greizgh/bookshelf/db"
	"gitlab.com/greizgh/bookshelf/server"
	"go.uber.org/zap"

	"github.com/adrg/xdg"
	"github.com/spf13/cobra"
)

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Serve OPDS feed and web interface for ebooks in directory",
	Run: func(cmd *cobra.Command, args []string) {
		port, err := cmd.Flags().GetString("port")
		if err != nil {
			zap.L().Fatal("Could not parse port", zap.Error(err))
		}
		bind, err := cmd.Flags().GetString("bind")
		if err != nil {
			zap.L().Fatal("Could not parse bind address", zap.Error(err))
		}

		listenAddr := bind + ":" + port

		database, err := db.GetDB()
		if err != nil {
			zap.L().Fatal("Failed to get database", zap.Error(err))
		}
		defer database.Close()
		bookStore := db.New(database)

		server.Serve(listenAddr, bookStore, cover.New(xdg.DataHome))
	},
}

func init() {
	serveCmd.Flags().StringP("port", "p", "8080", "listening port")
	serveCmd.Flags().StringP("bind", "b", "127.0.0.1", "listening address")
}
