// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package ebook

import (
	"os"
	"testing"
)

func TestFindEpubsInNonDirectory(t *testing.T) {
	_, err := FindEpubs("nonexistingdirectory")
	if err == nil {
		t.Error("visiting non existing directory should fail")
	}
}

func TestFindEpubs(t *testing.T) {
	parentDir, err := os.MkdirTemp("", "bookshelf-")
	if err != nil {
		t.Error(err)
	}
	defer os.RemoveAll(parentDir)

	testFiles := [2]string{"bookshelf-*.epub", "gopds-*.png"}
	var epubFile string

	for _, name := range testFiles {
		file, err := os.CreateTemp(parentDir, name)
		if err != nil {
			t.Error(err)
		}
		if epubFile == "" {
			epubFile = file.Name()
		}
		defer os.Remove(file.Name())
	}

	files, err := FindEpubs(parentDir)
	if err != nil {
		t.Error(err)
	}
	if files[0] != epubFile {
		t.Error("did not find epub")
	}
}
