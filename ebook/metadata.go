// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package ebook

import (
	"fmt"
	"image"
	"os"
	"time"

	// Allow to decode both jpeg and png covers
	_ "image/jpeg"
	_ "image/png"

	"github.com/geek1011/BookBrowser/formats"
	// Allow to extract metadata from epubs
	_ "github.com/geek1011/BookBrowser/formats/epub"
	"github.com/microcosm-cc/bluemonday"
)

// BookWithCover wraps a Book to add cover image information
type BookWithCover struct {
	*Book
	Cover image.Image
}

// MetadataLoader describe objects able to retrieve metadata from a file
type MetadataLoader interface {
	Load(filepath string) (*BookWithCover, error)
	GetModTime(filepath string) (time.Time, error)
}

var p *bluemonday.Policy

func init() {
	p = bluemonday.StrictPolicy()
}

// EpubLoader is a MetadataLoader extracting metadata from EPUB files
type EpubLoader struct{}

// Load will extract epub metadata
func (loader *EpubLoader) Load(path string) (*BookWithCover, error) {
	info, err := formats.Load(path)
	if err != nil {
		return nil, fmt.Errorf("Failed to retrieve metadata: %w", err)
	}

	book := &Book{
		Path:        path,
		ModTime:     info.Book().ModTime,
		Title:       info.Book().Title,
		Author:      info.Book().Author,
		Description: p.Sanitize(info.Book().Description),
		Series:      info.Book().Series,
		SeriesIndex: int(info.Book().SeriesIndex),
	}

	var cover image.Image

	if info.HasCover() {
		cover, err = info.GetCover()
		if err != nil {
			return nil, err
		}
	}

	return &BookWithCover{
		book,
		cover,
	}, nil
}

// GetModTime returns file modification time, without actually loading the epub.
func (loader *EpubLoader) GetModTime(path string) (time.Time, error) {
	file, err := os.Open(path)
	if err != nil {
		return time.Time{}, err
	}
	defer file.Close()

	fileinfo, err := file.Stat()
	if err != nil {
		return time.Time{}, err
	}

	return fileinfo.ModTime(), nil
}
