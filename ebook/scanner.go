// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package ebook

import (
	"os"
	"path/filepath"
)

const epubExtension = ".epub"

// FindEpubs scans a directory recursively and returns contained epub paths
func FindEpubs(directoryPath string) ([]string, error) {
	files := []string{}

	err := filepath.Walk(directoryPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if filepath.Ext(path) != epubExtension {
			return nil
		}
		absolute, err := filepath.Abs(path)
		if err != nil {
			return err
		}
		files = append(files, absolute)
		return nil
	})

	return files, err
}
