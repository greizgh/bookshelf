// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package server

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/greizgh/bookshelf/ebook"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestIndex(t *testing.T) {
	bookStore := new(mockBookStore)
	coverStore := new(mockCoverStore)

	bookStore.On("Recent").Return([]ebook.Book{{Title: "Book Title"}}, nil).Once()

	app := newWebApp(bookStore, coverStore)

	req, err := http.NewRequest("GET", "/", nil)
	require.Nil(t, err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.index)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code, "Index should respond successfully")

	assert.Contains(t, rr.Body.String(), "Book Title", "Index should display recent books")
}

func TestDetail(t *testing.T) {
	bookStore := new(mockBookStore)
	coverStore := new(mockCoverStore)

	bookStore.On("Get", "fb100d16-d822-417b-bda2-55e367b72ab2").Return(&ebook.Book{ID: "fb100d16-d822-417b-bda2-55e367b72ab2", Title: "Book Title", Description: "lorem ipsum"}, nil).Once()

	app := newWebApp(bookStore, coverStore)

	req, err := http.NewRequest("GET", "/book/fb100d16-d822-417b-bda2-55e367b72ab2", nil)
	require.Nil(t, err)

	ctx := context.WithValue(req.Context(), bookIDKey, "fb100d16-d822-417b-bda2-55e367b72ab2")
	reqWithBookID := req.WithContext(ctx)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.detail)
	handler.ServeHTTP(rr, reqWithBookID)

	assert.Equal(t, http.StatusOK, rr.Code, "Book page should respond successfully")

	assert.Contains(t, rr.Body.String(), "lorem ipsum", "Detail page should show description")
}

func TestSearch(t *testing.T) {
	bookStore := new(mockBookStore)
	coverStore := new(mockCoverStore)

	bookStore.On("Search", "query").Return([]ebook.Book{{Title: "Book Title"}}, nil).Once()

	app := newWebApp(bookStore, coverStore)

	req, err := http.NewRequest("GET", "/search?term=query", nil)
	require.Nil(t, err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.search)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code, "Search should respond successfully")

	assert.Contains(t, rr.Body.String(), "Book Title", "Search should display matching books")
}
