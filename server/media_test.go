// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package server

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"testing"

	"gitlab.com/greizgh/bookshelf/ebook"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const bookID = "fb100d16-d822-417b-bda2-55e367b72ab2"

func TestDownload(t *testing.T) {
	bookStore := new(mockBookStore)
	coverStore := new(mockCoverStore)

	bookContent := []byte("This is the book content")

	file, err := os.CreateTemp("", "test-*.epub")
	require.Nil(t, err)
	defer os.Remove(file.Name())
	_, err = file.Write(bookContent)
	require.Nil(t, err)

	ebook := &ebook.Book{ID: bookID, Title: "Book Title", Path: file.Name()}

	bookStore.On("Get", bookID).Return(ebook, nil).Once()

	app := newWebApp(bookStore, coverStore)

	req, err := http.NewRequest("GET", "/download/"+bookID, nil)
	require.Nil(t, err)

	ctx := context.WithValue(req.Context(), bookIDKey, bookID)
	reqWithBookID := req.WithContext(ctx)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.download)
	handler.ServeHTTP(rr, reqWithBookID)

	filename := path.Base(file.Name())
	assert.Equal(t, http.StatusOK, rr.Code, "Download should respond successfully")
	assert.Equal(t, "application/epub+zip", rr.Header().Get("Content-Type"), "Content-Type should be epub")
	assert.Equal(t, fmt.Sprintf("attachment; filename=%q", filename), rr.Header().Get("Content-Disposition"), "Header should propose the original filename")
	assert.Equal(t, bookContent, rr.Body.Bytes(), "Downloaded data should be the book content")
}

func TestThumbnail(t *testing.T) {
	bookStore := new(mockBookStore)
	coverStore := new(mockCoverStore)

	rr := httptest.NewRecorder()

	coverStore.On("GetThumbnail", bookID, rr).Return(int64(0), nil).Once()

	app := newWebApp(bookStore, coverStore)

	req, err := http.NewRequest("GET", "/thumbnail/"+bookID+".png", nil)
	require.Nil(t, err)
	ctx := context.WithValue(req.Context(), bookIDKey, bookID)
	reqWithBookID := req.WithContext(ctx)

	handler := http.HandlerFunc(app.thumbnail)
	handler.ServeHTTP(rr, reqWithBookID)

	assert.Equal(t, http.StatusOK, rr.Code, "Thumbnail should be served")
	assert.Equal(t, "image/png", rr.Header().Get("Content-Type"), "Thumbnails are PNG")
	assert.Greater(t, rr.Body.Len(), 0, "Thumbnail content should be sent")
}

func TestCover(t *testing.T) {
	bookStore := new(mockBookStore)
	coverStore := new(mockCoverStore)

	rr := httptest.NewRecorder()

	coverStore.On("GetCover", bookID, rr).Return(int64(0), nil).Once()

	app := newWebApp(bookStore, coverStore)

	req, err := http.NewRequest("GET", "/cover/"+bookID+".png", nil)
	require.Nil(t, err)
	ctx := context.WithValue(req.Context(), bookIDKey, bookID)
	reqWithBookID := req.WithContext(ctx)

	handler := http.HandlerFunc(app.cover)
	handler.ServeHTTP(rr, reqWithBookID)

	assert.Equal(t, http.StatusOK, rr.Code, "Thumbnail should be served")
	assert.Equal(t, "image/png", rr.Header().Get("Content-Type"), "Thumbnails are PNG")
	assert.Greater(t, rr.Body.Len(), 0, "Thumbnail content should be sent")
}
