// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package server

import (
	"net/http"

	"gitlab.com/greizgh/bookshelf/ebook"
)

type listParams struct {
	Books  []ebook.Book
	Search string
	Title  string
}

func (app *webApp) index(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	books, err := app.bookStore.Recent()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	renderTemplate(w, "list.html", &listParams{Books: books, Title: "New publications"})
}

func (app *webApp) detail(w http.ResponseWriter, r *http.Request) {
	id := r.Context().Value(bookIDKey).(string)
	book := app.getBookOrFail(id, w)
	if book == nil {
		return
	}
	renderTemplate(w, "detail.html", book)
}

func (app *webApp) search(w http.ResponseWriter, r *http.Request) {
	term := r.URL.Query().Get("term")
	books, err := app.bookStore.Search(term)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	renderTemplate(w, "list.html", &listParams{Books: books, Search: term, Title: term})
}
