// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package server

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/greizgh/bookshelf/ebook"

	"github.com/bradleyjkemp/cupaloy"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var colLastMod time.Time

func init() {
	colLastMod = time.Date(2020, 11, 12, 13, 14, 15, 16, time.UTC)
}

func TestOPDSRoot(t *testing.T) {
	bookStore := new(mockBookStore)
	coverStore := new(mockCoverStore)

	app := newWebApp(bookStore, coverStore)

	req, err := http.NewRequest("GET", "/opds", nil)
	require.Nil(t, err)
	ctx := context.WithValue(req.Context(), lastModKey, colLastMod)
	reqWithLastMod := req.WithContext(ctx)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.opds)
	handler.ServeHTTP(rr, reqWithLastMod)

	assert.Equal(t, http.StatusOK, rr.Code, "OPDS root should respond successfully")

	cupaloy.SnapshotT(t, rr.Body.String())
}

func TestOPDSNew(t *testing.T) {
	bookStore := new(mockBookStore)
	coverStore := new(mockCoverStore)

	bookStore.On("Recent").Return([]ebook.Book{{ID: "uuid", Title: "Book Title", Author: "Doe, John", Description: "Some test data"}}, nil).Once()

	app := newWebApp(bookStore, coverStore)

	req, err := http.NewRequest("GET", "/opds/new.xml", nil)
	require.Nil(t, err)
	ctx := context.WithValue(req.Context(), lastModKey, colLastMod)
	reqWithLastMod := req.WithContext(ctx)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.opdsNew)
	handler.ServeHTTP(rr, reqWithLastMod)

	assert.Equal(t, http.StatusOK, rr.Code, "recent OPDS should respond successfully")

	cupaloy.SnapshotT(t, rr.Body.String())
}

func TestOPDSSearch(t *testing.T) {
	bookStore := new(mockBookStore)
	coverStore := new(mockCoverStore)

	bookStore.On("Search", "query").Return([]ebook.Book{{ID: "uuid", Title: "Book Title", Author: "Doe, John", Description: "Some test data"}}, nil).Once()

	app := newWebApp(bookStore, coverStore)

	req, err := http.NewRequest("GET", "/opds/search.xml?term=query", nil)
	require.Nil(t, err)
	ctx := context.WithValue(req.Context(), lastModKey, colLastMod)
	reqWithLastMod := req.WithContext(ctx)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.opdsSearch)
	handler.ServeHTTP(rr, reqWithLastMod)

	assert.Equal(t, http.StatusOK, rr.Code, "recent OPDS should respond successfully")

	cupaloy.SnapshotT(t, rr.Body.String())
}
