// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package server

import (
	"crypto/md5" //#nosec used to identify search terms
	"encoding/hex"
	"encoding/xml"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/greizgh/bookshelf/ebook"
	"gitlab.com/greizgh/bookshelf/opds"
)

const (
	idRoot = "urn:bookshelf:root"
	idNew  = "urn:bookshelf:recent"

	urlRoot = "/opds"
	urlNew  = "/opds/new.xml"
)

var bookshelfAuthor = &opds.Author{
	Name: "SOPDS",
}

func (app *webApp) opds(w http.ResponseWriter, r *http.Request) {
	collectionLastMod := r.Context().Value(lastModKey).(time.Time)
	feed := newRootFeed(collectionLastMod)
	renderFeed(w, feed)
}

func (app *webApp) opdsNew(w http.ResponseWriter, r *http.Request) {
	books, err := app.bookStore.Recent()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	entries := make([]*opds.Entry, len(books))
	for i := range books {
		entries[i] = toEntry(&books[i])
	}

	builder := newResultFeedBuilder(idNew, "Recent publications", urlNew, entries)

	collectionLastMod := r.Context().Value(lastModKey).(time.Time)
	builder.WithUpdated(collectionLastMod)

	renderFeed(w, builder.Build())
}

func (app *webApp) opdsSearch(w http.ResponseWriter, r *http.Request) {
	term := r.URL.Query().Get("term")
	books, err := app.bookStore.Search(term)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	entries := make([]*opds.Entry, len(books))
	for i := range books {
		entries[i] = toEntry(&books[i])
	}

	termHash := md5.Sum([]byte(term)) //#nosec used to identify search terms

	builder := newResultFeedBuilder(
		"urn:bookshelf:search-"+hex.EncodeToString(termHash[:]),
		"Search results",
		"/opds/search.xml?term="+url.QueryEscape(term),
		entries,
	)

	collectionLastMod := r.Context().Value(lastModKey).(time.Time)
	builder.WithUpdated(collectionLastMod)

	renderFeed(w, builder.Build())
}

func renderFeed(w http.ResponseWriter, feed *opds.Feed) {
	buf, err := xml.MarshalIndent(feed, "", "\t")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/atom+xml")

	_, err = w.Write([]byte(xml.Header))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = w.Write(buf)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func newRootFeed(updated time.Time) *opds.Feed {
	links := []*opds.Link{
		{Rel: "self", Href: urlRoot, Type: opds.NavigationLink},
		{Rel: "start", Href: urlRoot, Type: opds.NavigationLink},
	}

	entries := []*opds.Entry{
		{
			ID:      idNew,
			Title:   "New publications",
			Content: &opds.Content{Content: "Recent books added to the collection", Type: "text"},
			Links: []*opds.Link{
				{Rel: "http://opds-spec.org/sort/new", Type: opds.AcquisitionLink, Href: urlNew},
			},
		},
	}

	builder := opds.NewFeedBuilder()
	builder.WithType(opds.Navigation).WithID(idRoot).WithAuthor(bookshelfAuthor).WithTitle("SOPDS Catalog")
	builder.WithLinks(links).WithEntries(entries)
	builder.WithUpdated(updated)

	return builder.Build()
}

func toEntry(book *ebook.Book) *opds.Entry {
	return &opds.Entry{
		ID:    "urn:uuid:" + book.ID,
		Title: book.Title,
		Content: &opds.Content{
			Content: book.Description,
			Type:    "text",
		},
		Authors: []*opds.Author{
			{Name: book.Author},
		},
		Updated: book.ModTime,
		Links: []*opds.Link{
			{
				Rel:  "http://opds-spec.org/acquisition",
				Type: "application/epub+zip",
				Href: "/download/" + book.ID,
			},
			{
				Rel:  "http://opds-spec.org/image",
				Type: "image/png",
				Href: "/cover/" + book.ID + ".png",
			},
			{
				Rel:  "http://opds-spec.org/image/thumbnail",
				Type: "image/png",
				Href: "/thumbnail/" + book.ID + ".png",
			},
		},
	}
}

func newResultFeedBuilder(id string, title string, url string, entries []*opds.Entry) *opds.FeedBuilder {
	builder := opds.NewFeedBuilder().WithType(opds.Acquisition)
	builder.WithID(id)
	builder.WithAuthor(bookshelfAuthor)
	builder.WithTitle(title)
	builder.WithLinks(
		[]*opds.Link{
			{Rel: "self", Href: url, Type: opds.AcquisitionLink},
			{Rel: "start", Href: urlRoot, Type: opds.NavigationLink},
			{Rel: "up", Href: urlRoot, Type: opds.NavigationLink},
		},
	)
	builder.WithEntries(entries)

	return builder
}
