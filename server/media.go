// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package server

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path"

	"go.uber.org/zap"
)

func (app *webApp) download(w http.ResponseWriter, r *http.Request) {
	id := r.Context().Value(bookIDKey).(string)
	book := app.getBookOrFail(id, w)
	if book == nil {
		return
	}
	file, err := os.Open(book.Path)
	if err != nil {
		http.Error(w, "Failed to load ebook", http.StatusInternalServerError)
		return
	}
	defer file.Close()

	filename := path.Base(book.Path)

	w.Header().Set("Content-Type", "application/epub+zip")
	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%q", filename))
	_, err = io.Copy(w, file)
	if err != nil {
		zap.L().Warn("download failed", zap.Error(err))
	}
}

func (app *webApp) thumbnail(w http.ResponseWriter, r *http.Request) {
	id := r.Context().Value(bookIDKey).(string)

	w.Header().Set("Content-Type", "image/png")

	_, err := app.coverStore.GetThumbnail(id, w)
	if err != nil {
		http.Error(w, "Failed to load thumbnail", http.StatusInternalServerError)
	}
}

func (app *webApp) cover(w http.ResponseWriter, r *http.Request) {
	id := r.Context().Value(bookIDKey).(string)

	w.Header().Set("Content-Type", "image/png")

	_, err := app.coverStore.GetCover(id, w)
	if err != nil {
		http.Error(w, "Failed to load cover", http.StatusInternalServerError)
	}
}
