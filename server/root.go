// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

// Package server is responsible for exposing the collection over the web.
// The collection can be consumed from the web UI or the OPDS feed.
package server

import (
	"context"
	"database/sql"
	"embed"
	"html/template"
	"net/http"
	"strings"
	"time"

	"gitlab.com/greizgh/bookshelf/cover"
	"gitlab.com/greizgh/bookshelf/db"
	"gitlab.com/greizgh/bookshelf/ebook"
	"go.uber.org/zap"
)

//go:embed tmpl/*
var templateFS embed.FS

//go:embed assets/*
var assetsFS embed.FS

type contextKey int

const (
	bookIDKey contextKey = iota
	lastModKey
)

var templates *template.Template

// webApp wraps what's necessary for the server to expose the collection
type webApp struct {
	bookStore  db.BookStore
	coverStore cover.ImageStore
}

// Serve will start listening connections and serve html & OPDS content
func Serve(addr string, store *db.Store, covers cover.ImageStore) {
	initTemplates()
	app := &webApp{store, covers}

	mux := http.NewServeMux()

	mux.Handle("/assets/", http.FileServer(http.FS(assetsFS)))

	mux.HandleFunc("/", withCollectionLastMod(store, withCache(app.index)))
	mux.HandleFunc("/book/", withID("/book/", "", withBookLastMod(store, withCache(app.detail))))
	mux.HandleFunc("/download/", withID("/download/", "", app.download))
	mux.HandleFunc("/search", withCollectionLastMod(store, withCache(app.search)))
	mux.HandleFunc("/cover/", withID("/cover/", ".png", withBookLastMod(store, withCache(app.cover))))
	mux.HandleFunc("/thumbnail/", withID("/thumbnail/", ".png", withBookLastMod(store, withCache(app.thumbnail))))
	mux.HandleFunc("/opds", withCollectionLastMod(store, app.opds))
	mux.HandleFunc("/opds/new.xml", withCollectionLastMod(store, app.opdsNew))
	mux.HandleFunc("/opds/search.xml", withCollectionLastMod(store, app.opdsSearch))

	logger := zap.L()
	logger.Info("starting server", zap.String("address", addr))
	err := http.ListenAndServe(addr, mux)
	if err != nil {
		logger.Fatal("can't serve", zap.Error(err))
	}
}

// Fetch an ebook from DB or return 404
func (app *webApp) getBookOrFail(id string, w http.ResponseWriter) *ebook.Book {
	book, err := app.bookStore.Get(id)
	switch err {
	case nil:
		break
	case sql.ErrNoRows: // TODO: don't leak sql in server module
		http.Error(w, err.Error(), http.StatusNotFound)
		return nil
	default:
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return nil
	}
	return book
}

func renderTemplate(w http.ResponseWriter, tmpl string, params interface{}) {
	err := templates.ExecuteTemplate(w, tmpl, params)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func initTemplates() {
	tpl, err := template.ParseFS(templateFS, "tmpl/*")
	if err != nil {
		panic(err)
	}
	templates = tpl
}

// wraps a handler to provide book ID in context
func withID(prefix string, suffix string, next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id := r.URL.Path[len(prefix):]

		if !strings.HasSuffix(id, suffix) {
			http.Error(w, "Not found", http.StatusNotFound)
			return
		}
		id = id[:len(id)-len(suffix)]

		ctxWithID := context.WithValue(r.Context(), bookIDKey, id)
		rWithID := r.WithContext(ctxWithID)

		next(w, rWithID)
	}
}

// wraps a handler to provide book last modification date in context
func withBookLastMod(store db.BookStore, next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id := r.Context().Value(bookIDKey).(string)
		lastMod, err := store.GetLastModification(id)

		if err != nil {
			// treat all errors as not found
			http.Error(w, "Resource not found", http.StatusNotFound)
			return
		}

		w.Header().Set("Last-Modified", lastMod.UTC().Format(http.TimeFormat))

		ctxWithLastMod := context.WithValue(r.Context(), lastModKey, lastMod)
		rWithLastMod := r.WithContext(ctxWithLastMod)

		next(w, rWithLastMod)
	}
}

// wraps a handler to provide collection last modification date in context
func withCollectionLastMod(store db.CollectionStore, next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		lastMod, err := store.GetCollectionLastModification()
		if err != nil {
			http.Error(w, "Oops, something went wrong", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Last-Modified", lastMod.UTC().Format(http.TimeFormat))

		ctxWithLastMod := context.WithValue(r.Context(), lastModKey, lastMod)
		rWithLastMod := r.WithContext(ctxWithLastMod)

		next(w, rWithLastMod)
	}
}

// wraps a handler to set cache header according to last-modification date
func withCache(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "public, max-age=2592000, stale-while-revalidate=86400")
		lastMod := r.Context().Value(lastModKey).(time.Time).Truncate(time.Second)

		modifiedSince := r.Header.Get("If-Modified-Since")
		if modifiedSince == "" {
			next(w, r)
			return
		}

		d, err := time.Parse(http.TimeFormat, modifiedSince)
		if err != nil {
			next(w, r)
			return
		}

		if lastMod.After(d) {
			next(w, r)
			return
		}

		w.WriteHeader(http.StatusNotModified)
	}
}
