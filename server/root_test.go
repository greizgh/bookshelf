// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package server

import (
	"context"
	"image"
	"image/png"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/greizgh/bookshelf/cover"
	"gitlab.com/greizgh/bookshelf/db"
	"gitlab.com/greizgh/bookshelf/ebook"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func newWebApp(bookStore db.BookStore, coverStore cover.ImageStore) *webApp {
	initTemplates()
	return &webApp{
		bookStore,
		coverStore,
	}
}

type mockBookStore struct {
	mock.Mock
}

func (m *mockBookStore) Get(id string) (*ebook.Book, error) {
	args := m.Called(id)
	return args.Get(0).(*ebook.Book), nil
}
func (m *mockBookStore) GetLastModification(id string) (time.Time, error) {
	args := m.Called(id)
	return args.Get(0).(time.Time), nil
}
func (m *mockBookStore) Recent() ([]ebook.Book, error) {
	args := m.Called()
	return args.Get(0).([]ebook.Book), nil
}
func (m *mockBookStore) Search(query string) ([]ebook.Book, error) {
	args := m.Called(query)
	return args.Get(0).([]ebook.Book), nil
}

type mockCollectionStore struct {
	mock.Mock
}

func (m *mockCollectionStore) GetCollectionLastModification() (time.Time, error) {
	args := m.Called()
	return args.Get(0).(time.Time), nil
}

type mockCoverStore struct {
	mock.Mock
}

func (m *mockCoverStore) SaveCover(id string, img image.Image) error {
	args := m.Called(id, img)
	return args.Error(0)
}

func (m *mockCoverStore) GetCover(id string, w io.Writer) (int64, error) {
	img := image.NewRGBA(image.Rect(0, 0, 100, 100))
	_ = png.Encode(w, img)

	args := m.Called(id, w)
	return args.Get(0).(int64), args.Error(1)
}

func (m *mockCoverStore) GetThumbnail(id string, w io.Writer) (int64, error) {
	img := image.NewRGBA(image.Rect(0, 0, 10, 10))
	_ = png.Encode(w, img)

	args := m.Called(id, w)
	return args.Get(0).(int64), args.Error(1)
}

type mockHandler struct {
	mock.Mock
	lastReq *http.Request
}

func (m *mockHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	m.lastReq = r
	m.Called(w, r)
}

func TestWithId(t *testing.T) {
	next := &mockHandler{}
	next.On("ServeHTTP", mock.Anything, mock.Anything).Once()

	req, err := http.NewRequest("GET", "/cover/"+bookID+".png", nil)
	require.Nil(t, err)

	rr := httptest.NewRecorder()

	handler := withID("/cover/", ".png", next.ServeHTTP)
	handler(rr, req)

	id := next.lastReq.Context().Value(bookIDKey).(string)
	assert.Equal(t, bookID, id, "Book ID should be passed in context")

	next.AssertExpectations(t)
}

func TestWithBookLastMod(t *testing.T) {
	next := &mockHandler{}
	lastMod := time.Unix(1606142518, 0)

	next.On("ServeHTTP", mock.Anything, mock.Anything).Once()

	req, err := http.NewRequest("GET", "/cover/"+bookID+".png", nil)
	require.Nil(t, err)

	ctx := context.WithValue(req.Context(), bookIDKey, bookID)
	reqWithBookID := req.WithContext(ctx)

	rr := httptest.NewRecorder()

	bookStore := new(mockBookStore)
	bookStore.On("GetLastModification", bookID).Return(lastMod, nil).Once()

	handler := withBookLastMod(bookStore, next.ServeHTTP)
	handler(rr, reqWithBookID)

	next.AssertExpectations(t)
	bookStore.AssertExpectations(t)

	ctxLastMod := next.lastReq.Context().Value(lastModKey).(time.Time)
	assert.Equal(t, lastMod, ctxLastMod, "Context should hold the last modification time")

	assert.Equal(t, "Mon, 23 Nov 2020 14:41:58 GMT", rr.Header().Get("Last-Modified"), "Response should contain last modified date")
}

func TestWithCollectionLastMod(t *testing.T) {
	next := &mockHandler{}
	lastMod := time.Unix(1606142518, 0)

	next.On("ServeHTTP", mock.Anything, mock.Anything).Once()

	req, err := http.NewRequest("GET", "/cover/"+bookID+".png", nil)
	require.Nil(t, err)

	ctx := context.WithValue(req.Context(), bookIDKey, bookID)
	reqWithBookID := req.WithContext(ctx)

	rr := httptest.NewRecorder()

	collectionStore := new(mockCollectionStore)
	collectionStore.On("GetCollectionLastModification").Return(lastMod, nil).Once()

	handler := withCollectionLastMod(collectionStore, next.ServeHTTP)
	handler(rr, reqWithBookID)

	next.AssertExpectations(t)
	collectionStore.AssertExpectations(t)

	ctxLastMod := next.lastReq.Context().Value(lastModKey).(time.Time)
	assert.Equal(t, lastMod, ctxLastMod, "Context should hold the last modification time")

	assert.Equal(t, "Mon, 23 Nov 2020 14:41:58 GMT", rr.Header().Get("Last-Modified"), "Response should contain last modified date")
}

func TestWithoutCache(t *testing.T) {
	next := &mockHandler{}
	lastMod := time.Unix(1000, 0)

	next.On("ServeHTTP", mock.Anything, mock.Anything).Once()

	req, err := http.NewRequest("GET", "/cover/"+bookID+".png", nil)
	require.Nil(t, err)

	ctx := context.WithValue(req.Context(), lastModKey, lastMod)
	reqWithLastMod := req.WithContext(ctx)

	rr := httptest.NewRecorder()

	handler := withCache(next.ServeHTTP)
	handler(rr, reqWithLastMod)

	next.AssertExpectations(t)
}

// Unix(2000) is after Unix(1000), so cache is still valid
func TestWithCache(t *testing.T) {
	next := &mockHandler{}
	lastMod := time.Unix(1000, 0)

	req, err := http.NewRequest("GET", "/cover/"+bookID+".png", nil)
	require.Nil(t, err)

	ctx := context.WithValue(req.Context(), lastModKey, lastMod)
	reqWithLastMod := req.WithContext(ctx)

	reqWithLastMod.Header.Add("If-Modified-Since", time.Unix(2000, 0).Format(http.TimeFormat))

	rr := httptest.NewRecorder()

	handler := withCache(next.ServeHTTP)
	handler(rr, reqWithLastMod)

	next.AssertExpectations(t)
	assert.Equal(t, http.StatusNotModified, rr.Code, "Response code should be 304")
}

// Cache header doesn't contain sub second time information, make sure we don't compare it
func TestWithCacheRounding(t *testing.T) {
	next := &mockHandler{}
	lastMod := time.Date(2020, 11, 19, 20, 04, 40, 10, time.UTC) // header + 10ns

	req, err := http.NewRequest("GET", "/cover/"+bookID+".png", nil)
	require.Nil(t, err)

	ctx := context.WithValue(req.Context(), lastModKey, lastMod)
	reqWithLastMod := req.WithContext(ctx)

	reqWithLastMod.Header.Add("If-Modified-Since", "Thu, 19 Nov 2020 20:04:40 GMT")

	rr := httptest.NewRecorder()

	handler := withCache(next.ServeHTTP)
	handler(rr, reqWithLastMod)

	next.AssertExpectations(t)
	assert.Equal(t, http.StatusNotModified, rr.Code, "Response code should be 304")
}

// Unix(1000) is before Unix(10000), so cache is invalid
func TestChangedResource(t *testing.T) {
	next := &mockHandler{}
	lastMod := time.Unix(10000, 0)

	next.On("ServeHTTP", mock.Anything, mock.Anything).Once()

	req, err := http.NewRequest("GET", "/cover/"+bookID+".png", nil)
	require.Nil(t, err)

	ctx := context.WithValue(req.Context(), lastModKey, lastMod)
	reqWithLastMod := req.WithContext(ctx)

	reqWithLastMod.Header.Add("If-Modified-Since", time.Unix(1000, 0).Format(http.TimeFormat))

	rr := httptest.NewRecorder()

	handler := withCache(next.ServeHTTP)
	handler(rr, reqWithLastMod)

	next.AssertExpectations(t)
}
