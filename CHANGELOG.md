# Changelog

## Unreleased

## [0.5.0] 2023-03-24
### Changed
- downloaded files use the original file name instead of identifier
- replaced logger library with uber/zap

## [0.4.0] 2023-02-17
### Changed
- switched to pure go sqlite driver

## [0.3.0] 2021-12-20
### Fixed
- properly handle hyphen in search queries
- properly escape apostrophe in descriptions
### Added
- improve indexation speed
- debug output during indexation
- verbose option (default to false)
### Changed
- use structured logging
### Removed
- progress bar during indexation

## [0.2.0] 2021-01-01
### Changed
- Changed module path from `bookshelf` to `gitlab.com/greizgh/bookshelf`

## [0.1.0] 2020-11-26
### Added
- Serve browsable book collection
- Serve OPDS feed for new content and search
- Incremental indexing of the library
