// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package opds

import (
	"encoding/xml"
	"testing"

	"github.com/bradleyjkemp/cupaloy"
	"github.com/stretchr/testify/require"
)

var linkTestCases = map[string]Link{
	"HrefOnly": {
		Href: "/opds/test.xml",
	},
	"AllAttrs": {
		Rel:  "self",
		Href: "/opds/test.xml",
		Type: "application/atom+xml;profile=opds-catalog;kind=acquisition",
	},
}

func TestLinkMarshalling(t *testing.T) {
	for testName, link := range linkTestCases {
		t.Run(testName, func(t *testing.T) {
			assertSnapshot(t, link)
		})
	}
}

var authorTestCases = map[string]Author{
	"SimpleAuthor": {
		Name: "Test Dummy",
	},
	"AuthorWithURI": {
		Name: "Test Dummy 2",
		URI:  "/test-dummy",
	},
}

func TestAuthorMarshalling(t *testing.T) {
	for testName, author := range authorTestCases {
		t.Run(testName, func(t *testing.T) {
			assertSnapshot(t, author)
		})
	}
}

func assertSnapshot(t *testing.T, arg interface{}) {
	out, err := xml.MarshalIndent(arg, "", "\t")
	require.Nil(t, err, "Failed to marshal author")

	cupaloy.SnapshotT(t, string(out))
}
