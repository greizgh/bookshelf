// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

// Package opds provides structures to represent valid OPDS feeds
// According to https://specs.opds.io/opds-1.2
package opds

import (
	"encoding/xml"
	"time"
)

// FeedType denotes the OPDS type, being either a navigation or acquisition feed
// See https://specs.opds.io/opds-1.2#2-opds-catalog-feed-documents
type FeedType string

const (
	// Navigation feed: https://specs.opds.io/opds-1.2#22-navigation-feeds
	Navigation FeedType = "navigation"
	// Acquisition feed: https://specs.opds.io/opds-1.2#23-acquisition-feeds
	Acquisition = "acquisition"
)

// Feed represents an OPDS feed, use a FeedBuilder to properly create namespaced feed
type Feed struct {
	XMLName       xml.Name  `xml:"feed"`
	Xmlns         string    `xml:"xmlns,attr"`
	Xmlnsdc       string    `xml:"xmlns:dc,attr,omitempty"`
	Xmlnbookshelf string    `xml:"xmlns:opds,attr,omitempty"`
	Title         string    `xml:"title"`
	ID            string    `xml:"id"`
	Updated       time.Time `xml:"updated"`
	Links         []*Link   `xml:"links,omitempty"`
	Author        *Author   `xml:"author,omitempty"`
	Entries       []*Entry  `xml:",omitempty"`
}

// FeedBuilder allow to build a Feed while enforcing few invariants
type FeedBuilder struct {
	feed *Feed
}

// WithID sets the feed's ID
func (builder *FeedBuilder) WithID(id string) *FeedBuilder {
	builder.feed.ID = id
	return builder
}

// WithTitle sets the feed's title
func (builder *FeedBuilder) WithTitle(title string) *FeedBuilder {
	builder.feed.Title = title
	return builder
}

// WithUpdated sets the feed's updated date
func (builder *FeedBuilder) WithUpdated(updated time.Time) *FeedBuilder {
	builder.feed.Updated = updated
	return builder
}

// WithLinks adds links to the feed
func (builder *FeedBuilder) WithLinks(links []*Link) *FeedBuilder {
	builder.feed.Links = links
	return builder
}

// WithAuthor sets the feed's author
func (builder *FeedBuilder) WithAuthor(author *Author) *FeedBuilder {
	builder.feed.Author = author
	return builder
}

// WithEntries adds entries to the feed
func (builder *FeedBuilder) WithEntries(entries []*Entry) *FeedBuilder {
	builder.feed.Entries = entries
	return builder
}

// WithType sets the feed's type, and may update the XML namespace accordingly
func (builder *FeedBuilder) WithType(kind FeedType) *FeedBuilder {
	switch kind {
	case Acquisition:
		builder.feed.Xmlnsdc = "http://purl.org/dc/terms/"
		builder.feed.Xmlnbookshelf = "http://opds-spec.org/2010/catalog"
	}
	return builder
}

// Build will return the feed as configured by the FeedBuilder
func (builder *FeedBuilder) Build() *Feed {
	return builder.feed
}

// NewFeedBuilder creates a FeedBuilder ready to use
func NewFeedBuilder() *FeedBuilder {
	return &FeedBuilder{
		feed: &Feed{
			Xmlns: "http://www.w3.org/2005/Atom",
		},
	}
}
