// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package opds

import "testing"

var entryTestCases = map[string]Entry{
	"WithContent": {
		ID:      "urn:bookshelf:test",
		Content: &Content{Content: "This is a test entry", Type: "text"},
	},
	"WithAuthors": {
		Authors: []*Author{{Name: "Test"}, {Name: "other"}},
	},
	"WithLinks": {
		ID:    "urn:bookshelf:test",
		Title: "Test feed",
		Links: []*Link{{Rel: "self", Href: "/opds/test.xml"}, {Rel: "parent", Href: "/opds.xml"}},
	},
}

func TestEntryMarshalling(t *testing.T) {
	for testName, entry := range entryTestCases {
		t.Run(testName, func(t *testing.T) {
			assertSnapshot(t, entry)
		})
	}
}
