// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package opds

import (
	"encoding/xml"
	"time"
)

// Entry represents an entry of an OPDS feed
type Entry struct {
	XMLName xml.Name  `xml:"entry"`
	ID      string    `xml:"id"`
	Title   string    `xml:"title"`
	Links   []*Link   `xml:",omitempty"`
	Updated time.Time `xml:"updated"`
	Content *Content  `xml:",omitempty"`
	Summary string    `xml:"summary,omitempty"`
	Authors []*Author `xml:",omitempty"`
}
