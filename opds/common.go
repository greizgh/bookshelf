// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package opds

import "encoding/xml"

const (
	// NavigationLink points to a navigation feed: https://specs.opds.io/opds-1.2#22-navigation-feeds
	NavigationLink = "application/atom+xml;profile=opds-catalog;kind=navigation"
	// AcquisitionLink points to an acquisition feed: https://specs.opds.io/opds-1.2#23-acquisition-feeds
	AcquisitionLink = "application/atom+xml;profile=opds-catalog;kind=acquisition"
)

// Link points to resources such as feed or entry
type Link struct {
	XMLName xml.Name `xml:"link"`
	Rel     string   `xml:"rel,attr,omitempty"`
	Href    string   `xml:"href,attr"`
	Type    string   `xml:"type,attr,omitempty"`
}

// Author of a resource
type Author struct {
	XMLName xml.Name `xml:"author"`
	Name    string   `xml:"name"`
	URI     string   `xml:"uri,omitempty"`
}

// Content of an entry
type Content struct {
	XMLName xml.Name `xml:"content"`
	Content string   `xml:",cdata"`
	Type    string   `xml:"type,attr"`
}
