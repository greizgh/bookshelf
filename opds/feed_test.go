// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package opds

import (
	"encoding/xml"
	"testing"
	"time"

	"github.com/bradleyjkemp/cupaloy"
	"github.com/stretchr/testify/require"
)

func TestEmptyFeed(t *testing.T) {
	out, err := xml.MarshalIndent(NewFeedBuilder().WithID("urn:bookshelf:main").Build(), "", "\t")
	require.Nil(t, err, "Failed to marshal new feed")

	cupaloy.SnapshotT(t, string(out))
}

func TestCompleteFeed(t *testing.T) {
	builder := NewFeedBuilder()
	builder.WithID("urn:bookshelf:main")
	builder.WithTitle("Test feed")
	builder.WithType(Acquisition)
	builder.WithUpdated(time.Date(2020, 11, 14, 18, 25, 12, 0, time.UTC))
	builder.WithAuthor(&Author{Name: "Test Dummy"})
	builder.WithLinks([]*Link{{Href: "/test.xml"}, {Href: "/opds", Rel: "start", Type: "application/atom+xml;profile=opds-catalog;kind=navigation"}})
	builder.WithEntries([]*Entry{
		{ID: "test", Title: "some dummy entry"},
		{Title: "test entry", Content: &Content{Content: "Lorem ipsum", Type: "text"}},
	})

	out, err := xml.MarshalIndent(builder.Build(), "", "\t")
	require.Nil(t, err, "Failed to marshal new feed")

	cupaloy.SnapshotT(t, string(out))
}
