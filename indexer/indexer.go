// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

// Package indexer holds the logic for ebook indexation
package indexer

import (
	"image"
	"runtime"

	"gitlab.com/greizgh/bookshelf/cover"
	"gitlab.com/greizgh/bookshelf/db"
	"gitlab.com/greizgh/bookshelf/ebook"
	"go.uber.org/zap"
)

// IndexStat represents indexation outcomes
type IndexStat int

const (
	// Success relates to successfully indexed books
	Success IndexStat = iota
	// Skipped relates to unchanged books
	Skipped
	// Error means something went wrong during indexation
	Error
)

// Indexer handles book indexation, it basically holds references to underlying stores
type Indexer struct {
	bookStore  db.BookIndex
	loader     ebook.MetadataLoader
	coverStore cover.ImageStore
}

// New returns an Indexer created with given stores and loaders
func New(store db.BookIndex, loader ebook.MetadataLoader, coverStore cover.ImageStore) *Indexer {
	return &Indexer{
		bookStore:  store,
		loader:     loader,
		coverStore: coverStore,
	}
}

type writableCover struct {
	id  string
	img image.Image
}

// Index ebook for each given path
func (indexer *Indexer) Index(filePaths []string) map[IndexStat]int {
	files := make(chan string)
	defer close(files)
	metadata := make(chan *ebook.BookWithCover)
	defer close(metadata)
	covers := make(chan *writableCover)
	defer close(covers)
	report := make(chan IndexStat)
	defer close(report)

	for i := 0; i < runtime.NumCPU()/2+1; i++ {
		go indexer.extractMetadata(files, metadata, report)
		go indexer.indexBook(metadata, covers, report)
		go indexer.saveCover(covers, report)
	}

	r := make(chan map[IndexStat]int)
	defer close(r)
	go processReport(len(filePaths), report, r)

	for _, path := range filePaths {
		files <- path
	}

	return <-r
}

func (indexer *Indexer) extractMetadata(files <-chan string, metadata chan<- *ebook.BookWithCover, report chan<- IndexStat) {
	for path := range files {
		logger := zap.L().With(zap.String("path", path))

		// Ignore any error when reading ModTime and proceed with import
		if lastChange, err := indexer.loader.GetModTime(path); err == nil {
			if changed, err := indexer.bookStore.HasChanged(path, lastChange); err == nil && !changed {
				logger.Debug("Book unchanged", zap.Time("modTime", lastChange))
				report <- Skipped
				continue
			}
		}

		book, err := indexer.loader.Load(path)
		if err != nil {
			report <- Error
			logger.Error("Failed to extract metadata", zap.Error(err))
			continue
		}

		metadata <- book
		logger.Debug("Successfully extracted metadata", zap.String("path", path))
	}
}

func (indexer *Indexer) indexBook(metadata <-chan *ebook.BookWithCover, covers chan<- *writableCover, report chan<- IndexStat) {
	for book := range metadata {
		logger := zap.L().With(zap.String("path", book.Path))
		id, err := indexer.bookStore.InsertBook(book.Book)
		if err != nil {
			report <- Error
			logger.Error("Failed to store book in database", zap.Error(err))
			continue
		}

		covers <- &writableCover{
			id:  id,
			img: book.Cover,
		}
		logger.Debug("Successfully indexed book", zap.String("bookId", id))
	}
}

func (indexer *Indexer) saveCover(covers <-chan *writableCover, report chan<- IndexStat) {
	for cover := range covers {
		if cover != nil && cover.img != nil {
			logger := zap.L().With(zap.String("bookId", cover.id))
			err := indexer.coverStore.SaveCover(cover.id, cover.img)
			if err != nil {
				logger.Warn("Failed to save cover image", zap.Error(err))
			} else {
				logger.Debug("Successfully saved cover")
			}
		}

		// Even if cover saving fails, the book is indexed
		report <- Success
	}
}

func processReport(total int, report <-chan IndexStat, result chan<- map[IndexStat]int) {
	results := make(map[IndexStat]int, 3)

	for i := 0; i < total; i++ {
		results[<-report]++
	}

	result <- results
}
