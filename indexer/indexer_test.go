// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package indexer

import (
	"errors"
	"image"
	"io"
	"testing"
	"time"

	"gitlab.com/greizgh/bookshelf/ebook"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockedBookStore struct {
	mock.Mock
}

func (m *MockedBookStore) InsertBook(book *ebook.Book) (string, error) {
	args := m.Called(book)
	return args.String(0), args.Error(1)
}

func (m *MockedBookStore) HasChanged(path string, lastMod time.Time) (bool, error) {
	args := m.Called(path, lastMod)
	return args.Bool(0), args.Error(1)
}

type MockedLoader struct {
	mock.Mock
}

func (m *MockedLoader) Load(filepath string) (*ebook.BookWithCover, error) {
	args := m.Called(filepath)
	return args.Get(0).(*ebook.BookWithCover), args.Error(1)
}

func (m *MockedLoader) GetModTime(filepath string) (time.Time, error) {
	args := m.Called(filepath)
	return args.Get(0).(time.Time), args.Error(1)
}

type MockedCoverStore struct {
	mock.Mock
}

func (m *MockedCoverStore) SaveCover(id string, img image.Image) error {
	args := m.Called(id, img)
	return args.Error(0)
}

func (m *MockedCoverStore) GetCover(id string, w io.Writer) (int64, error) {
	args := m.Called(id, w)
	return args.Get(0).(int64), args.Error(1)
}

func (m *MockedCoverStore) GetThumbnail(id string, w io.Writer) (int64, error) {
	args := m.Called(id, w)
	return args.Get(0).(int64), args.Error(1)
}

func TestIndexFiles(t *testing.T) {
	mockedBookStore := &MockedBookStore{}
	mockedLoader := &MockedLoader{}
	mockedCoverStore := &MockedCoverStore{}

	cover := image.NewRGBA(image.Rect(0, 0, 1, 1))

	book1 := &ebook.Book{Title: "book1"}
	book2 := &ebook.Book{Title: "book2"}
	book3 := &ebook.Book{Title: "book3"}
	withCover1 := &ebook.BookWithCover{Book: book1}
	withCover2 := &ebook.BookWithCover{Book: book2}
	withCover3 := &ebook.BookWithCover{Book: book3, Cover: cover}

	mockedLoader.On("GetModTime", mock.Anything).Times(4).Return(time.Time{}, errors.New("skip change"))

	mockedLoader.On("Load", "/test/1.epub").Return(withCover1, nil).Once()
	mockedLoader.On("Load", "/test/2.epub").Return(withCover1, errors.New("failed to load")).Once()
	mockedLoader.On("Load", "/test/3.epub").Return(withCover2, nil).Once()
	mockedLoader.On("Load", "/test/cover.epub").Return(withCover3, nil).Once()

	mockedBookStore.On("InsertBook", book3).Return("uuid-with-cover", nil).Once()
	mockedBookStore.On("InsertBook", book1).Return("uuid", nil).Once()
	mockedBookStore.On("InsertBook", book2).Return("", errors.New("failed to insert")).Once()

	mockedCoverStore.On("SaveCover", "uuid-with-cover", cover).Return(nil).Once()

	indexer := New(mockedBookStore, mockedLoader, mockedCoverStore)

	filePaths := []string{"/test/1.epub", "/test/2.epub", "/test/3.epub", "/test/cover.epub"}
	result := indexer.Index(filePaths)

	mockedLoader.AssertExpectations(t)
	mockedBookStore.AssertExpectations(t)
	mockedCoverStore.AssertExpectations(t)

	assert.Equal(t, 2, result[Error])
	assert.Equal(t, 2, result[Success])
	assert.Equal(t, 0, result[Skipped])
}

func TestSkipUnchanged(t *testing.T) {
	mockedBookStore := &MockedBookStore{}
	mockedLoader := &MockedLoader{}
	mockedCoverStore := &MockedCoverStore{}

	book1 := &ebook.Book{Title: "book1", Path: "/test/1.epub", ModTime: time.Unix(0, 0)}
	book2 := &ebook.Book{Title: "book2", Path: "/test/2.epub", ModTime: time.Unix(10, 0)}
	withCover1 := &ebook.BookWithCover{Book: book1}

	mockedLoader.On("GetModTime", book1.Path).Once().Return(book1.ModTime, nil)
	mockedLoader.On("GetModTime", book2.Path).Once().Return(book2.ModTime, nil)

	mockedLoader.On("Load", book1.Path).Return(withCover1, nil).Once()

	mockedBookStore.On("HasChanged", book1.Path, book1.ModTime).Return(true, nil).Once()
	mockedBookStore.On("HasChanged", book2.Path, book2.ModTime).Return(false, nil).Once()

	mockedBookStore.On("InsertBook", book1).Return("uuid", nil).Once()

	indexer := New(mockedBookStore, mockedLoader, mockedCoverStore)

	filePaths := []string{"/test/1.epub", "/test/2.epub"}
	results := indexer.Index(filePaths)

	mockedLoader.AssertExpectations(t)
	mockedBookStore.AssertExpectations(t)
	mockedCoverStore.AssertExpectations(t)

	assert.Equal(t, 1, results[Success])
	assert.Equal(t, 1, results[Skipped])
	assert.Equal(t, 0, results[Error])
}
