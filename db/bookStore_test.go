// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package db

import (
	"database/sql"
	"fmt"
	"testing"
	"time"

	"gitlab.com/greizgh/bookshelf/ebook"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestSearch(t *testing.T) {
	db, err := sql.Open("sqlite", ":memory:")
	require.NoError(t, err)
	err = prepareDB(db)
	require.NoError(t, err)

	store := New(db)

	books := []ebook.Book{
		{Title: "A test a day keep the doctor away", Author: "John Doe", Path: "inmem1", Description: "Hands-on testing & how it helps designing good software"},
		{Title: "Some great adventure", Author: "John Doe", Path: "inmem2", Description: "Follow the adventures of hero"},
	}

	for i := range books {
		_, err = store.InsertBook(&books[i])
		require.NoError(t, err, "book insertion should not fail")
	}

	// Search terms and their expected cardinality
	cases := map[string]int{
		"doctor":     1,
		"john":       2,
		"adventures": 1,
		"software":   1,
		"hands-on":   1,
		`partial"`:   0,
		`partial'`:   0,
	}

	for term, cardinality := range cases {
		result, err := store.Search(term)
		require.NoError(t, err, fmt.Sprintf("searching should not fail: %q", term))
		assert.Len(t, result, cardinality)
	}
}

func TestLastMod(t *testing.T) {
	db, err := sql.Open("sqlite", ":memory:")
	require.NoError(t, err)
	err = prepareDB(db)
	require.NoError(t, err)

	lastMod, err := time.Parse(time.RFC3339, "1985-04-12T23:20:50.52Z")
	require.NoError(t, err)

	store := New(db)

	book := &ebook.Book{
		Title:   "test",
		ModTime: lastMod,
	}

	id, err := store.InsertBook(book)
	require.NoError(t, err, "book insertion should not fail")

	d, err := store.GetLastModification(id)
	require.NoError(t, err)

	assert.Equal(t, lastMod, d, "stored mod_time should not be altered")
}
