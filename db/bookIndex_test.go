// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package db

import (
	"database/sql"
	"testing"
	"time"

	"gitlab.com/greizgh/bookshelf/ebook"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestHasChanged(t *testing.T) {
	db, err := sql.Open("sqlite", ":memory:")
	require.NoError(t, err)
	err = prepareDB(db)
	require.NoError(t, err)

	lastMod, err := time.Parse(time.RFC3339, "1985-04-12T23:20:50.52Z")
	require.NoError(t, err)
	recent, err := time.Parse(time.RFC3339, "2020-06-30T23:20:50.52Z")
	require.NoError(t, err)

	store := New(db)

	book := &ebook.Book{
		Title:   "test",
		ModTime: lastMod,
		Path:    "/mem/test.epub",
	}

	_, err = store.InsertBook(book)
	require.NoError(t, err, "book insertion should not fail")

	c, err := store.HasChanged(book.Path, lastMod)
	require.NoError(t, err)
	assert.False(t, c, "Book should not be seen as changed if last mod is the same")

	c, err = store.HasChanged(book.Path, recent)
	require.NoError(t, err)
	assert.True(t, c, "Book has changed if modtime is more recent")
}

func TestUnknownBookHasChanged(t *testing.T) {
	db, err := sql.Open("sqlite", ":memory:")
	require.NoError(t, err)
	err = prepareDB(db)
	require.NoError(t, err)

	lastMod, err := time.Parse(time.RFC3339, "1985-04-12T23:20:50.52Z")
	require.NoError(t, err)

	store := New(db)

	c, err := store.HasChanged("/test/new/book.epub", lastMod)
	require.NoError(t, err)
	assert.True(t, c, "A new book should always be marked as changed")
}

func TestBookUpdate(t *testing.T) {
	db, err := sql.Open("sqlite", ":memory:")
	require.NoError(t, err)
	err = prepareDB(db)
	require.NoError(t, err)

	store := New(db)

	book := &ebook.Book{
		Title:       "test",
		Author:      "John Doe",
		Description: "This is a simple book",
		ModTime:     time.Now(),
		Path:        "/mem/test.epub",
	}

	inserted, err := store.InsertBook(book)
	require.NoError(t, err, "book insertion should not fail")

	book.Title = "Actually something else"

	updated, err := store.InsertBook(book)
	require.NoError(t, err, "book update should not fail")

	assert.Equal(t, inserted, updated, "Updating a book should preserve its ID")
}
