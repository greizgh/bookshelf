// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package db

import (
	"database/sql"
	"strconv"
	"strings"
	"time"

	"gitlab.com/greizgh/bookshelf/ebook"
)

// BookStore provides a read access to the ebook index
type BookStore interface {
	// Retrieve recent books
	Recent() ([]ebook.Book, error)
	// Fetch a book by its ID
	Get(id string) (*ebook.Book, error)
	// Search an ebook by title, author or series
	Search(query string) ([]ebook.Book, error)
	GetLastModification(id string) (time.Time, error)
}

const fetchRecent = `
SELECT b.uuid, b.title, b.author, b.series, b.path, b.mod_time, b.series_index, b.description
FROM books AS b
ORDER BY b.mod_time desc
LIMIT 20;
`
const fetchByID = `
SELECT b.uuid, b.title, b.author, b.series, b.path, b.mod_time, b.series_index, b.description
FROM books AS b
WHERE b.uuid = ?;
`
const search = `
SELECT b.uuid, b.title, b.author, b.series, b.path, b.mod_time, b.series_index, b.description
from books as b
inner join books_fts as s on s.rowid = b.id
where books_fts match ? order by rank;
`
const getLastMod = `
SELECT b.mod_time
FROM books AS b
WHERE b.uuid = ?;
`

// Recent retrieves recent books
func (store *Store) Recent() ([]ebook.Book, error) {
	rows, err := store.Query(fetchRecent)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return getBookList(rows)
}

// Get returns the book with given ID
func (store *Store) Get(id string) (*ebook.Book, error) {
	var b ebook.Book
	row := store.QueryRow(fetchByID, id)
	err := row.Scan(&b.ID, &b.Title, &b.Author, &b.Series, &b.Path, &b.ModTime, &b.SeriesIndex, &b.Description)
	if err != nil {
		return nil, err
	}
	return &b, err
}

// Search returns books matching the query
// This is a full-text search
func (store *Store) Search(query string) ([]ebook.Book, error) {
	q := strings.ReplaceAll(query, `"`, "")
	rows, err := store.Query(search, strconv.Quote(q))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return getBookList(rows)
}

// GetLastModification returns the modification time for a given book
func (store *Store) GetLastModification(id string) (time.Time, error) {
	t := time.Now()
	row := store.QueryRow(getLastMod, id)
	err := row.Scan(&t)
	if err != nil {
		return t, err
	}
	return t, err
}

func getBookList(rows *sql.Rows) ([]ebook.Book, error) {
	var books []ebook.Book
	var err error

	for rows.Next() {
		var b ebook.Book

		err := rows.Scan(&b.ID, &b.Title, &b.Author, &b.Series, &b.Path, &b.ModTime, &b.SeriesIndex, &b.Description)
		if err != nil {
			return nil, err
		}

		books = append(books, b)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return books, nil
}
