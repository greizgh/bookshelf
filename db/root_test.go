// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package db

import (
	"database/sql"
	"regexp"
	"testing"
	"time"

	"gitlab.com/greizgh/bookshelf/ebook"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	_ "modernc.org/sqlite"
)

var UUIDRegex = regexp.MustCompile(`^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$`)

func TestDBPreparation(t *testing.T) {
	db, err := sql.Open("sqlite", ":memory:")
	require.NoError(t, err)

	assert.False(t, isDBSetup(db))
	err = prepareDB(db)
	require.NoError(t, err)
	assert.True(t, isDBSetup(db))
}

// Insert a book and retrieve it
func TestBookRoundTrip(t *testing.T) {
	db, err := sql.Open("sqlite", ":memory:")
	require.NoError(t, err)
	err = prepareDB(db)
	require.NoError(t, err)

	store := New(db)

	book := &ebook.Book{
		Title:  "DB Insertion testing",
		Author: "test",
		Path:   "in memory",
	}

	id, err := store.InsertBook(book)
	require.NoError(t, err, "book insertion should not fail")
	assert.Regexp(t, UUIDRegex, id, "books should be identified by UUID")

	recents, err := store.Recent()
	require.NoError(t, err, "recent books retrieving should not fail")

	assert.Len(t, recents, 1)

	singleBook, err := store.Get(id)
	require.NoError(t, err, "fetching by ID should not fail")

	assert.Equal(t, recents[0].ID, singleBook.ID)
}

func TestCollectionLastMod(t *testing.T) {
	db, err := sql.Open("sqlite", ":memory:")
	require.NoError(t, err)
	err = prepareDB(db)
	require.NoError(t, err)

	mod1, err := time.Parse(time.RFC3339, "1985-04-12T23:20:50.52Z")
	require.NoError(t, err)
	mod2, err := time.Parse(time.RFC3339, "1987-04-12T23:20:50.52Z")
	require.NoError(t, err)

	store := New(db)

	book1 := &ebook.Book{
		Title:   "test",
		ModTime: mod1,
	}
	book2 := &ebook.Book{
		Title:   "test",
		ModTime: mod2,
	}

	_, err = store.InsertBook(book1)
	require.NoError(t, err, "book insertion should not fail")
	_, err = store.InsertBook(book2)
	require.NoError(t, err, "book insertion should not fail")

	d, err := store.GetCollectionLastModification()
	require.NoError(t, err)

	assert.Equal(t, mod2, d, "collection's mod time should be the most recent of all mod_time")
}
