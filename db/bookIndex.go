// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package db

import (
	"database/sql"
	"html"
	"time"

	"gitlab.com/greizgh/bookshelf/ebook"

	"github.com/google/uuid"
)

// BookIndex provides write access to the ebook index
type BookIndex interface {
	// Insert a single book in the index
	InsertBook(book *ebook.Book) (string, error)
	// Check if book should be indexed, based on last modification time
	HasChanged(path string, lastMod time.Time) (bool, error)
}

const upsertStatement = `
INSERT INTO books (uuid, title, author, description, series, path, mod_time, series_index)
VALUES (?, ?, ?, ?, ?, ?, ?, ?)
ON CONFLICT(path)
DO UPDATE SET title=excluded.title, author=excluded.author, description=excluded.description, series=excluded.series, mod_time=excluded.mod_time, series_index=excluded.series_index;
`

const getLastModByPath = `SELECT mod_time FROM books WHERE path = ?`

// UUID namespace used to generate deterministic UUIDs from book path
const namespace = "24432c53-a563-4bff-90f3-cdfab67365f9"

var bookSpace uuid.UUID

func init() {
	var err error
	bookSpace, err = uuid.Parse(namespace)
	if err != nil {
		panic(err)
	}
}

// InsertBook will store a single book in the index
func (store *Store) InsertBook(book *ebook.Book) (string, error) {
	store.writeLock.Lock()
	id := uuid.NewSHA1(bookSpace, []byte(book.Path))
	cleanDescription := html.UnescapeString(book.Description)
	_, err := store.Exec(
		upsertStatement,
		id,
		book.Title,
		book.Author,
		cleanDescription,
		book.Series,
		book.Path,
		book.ModTime,
		book.SeriesIndex,
	)
	store.writeLock.Unlock()

	return id.String(), err
}

// HasChanged will return false if the book is known and its modification date
// is not more recent that what is stored in the index.
// This will return true for any new book (not present in the index).
func (store *Store) HasChanged(path string, lastMod time.Time) (bool, error) {
	var t time.Time
	row := store.QueryRow(getLastModByPath, path)
	err := row.Scan(&t)
	if err == sql.ErrNoRows {
		return true, nil
	}
	if err != nil {
		return true, err
	}
	return lastMod.After(t), err
}
