// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package db

import (
	"database/sql"
	"sync"
	"time"

	"github.com/adrg/xdg"
	// sqlite driver
	_ "modernc.org/sqlite"
)

const createBooksTable = `
CREATE TABLE books (
	id INTEGER primary key,
	uuid TEXT NOT NULL,
	title TEXT NOT NULL,
	author TEXT NOT NULL,
	description TEXT,
	series TEXT,
	series_index INTEGER,
	path TEXT NOT NULL,
	mod_time TIMESTAMP NOT NULL
);
`

const createSearchTable = `
CREATE VIRTUAL TABLE books_fts USING fts5(
	uuid UNINDEXED,
	title,
	author,
	description,
	series,
	content = 'books',
	content_rowid = 'id'
);
`

const createUniquePathIndex = `
CREATE UNIQUE INDEX idx_book_unique_path ON books (path);
`

const createUniqueUUIDIndex = `
CREATE UNIQUE INDEX idx_book_uuid ON books (uuid);
`

const createInsertTrigger = `
CREATE TRIGGER books_ai AFTER INSERT ON books
	BEGIN
		INSERT INTO books_fts (rowid, uuid, title, author, description, series)
		VALUES (new.id, new.uuid, new.title, new.author, new.description, new.series);
	END;
`
const createDeleteTrigger = `
CREATE TRIGGER books_ad AFTER DELETE ON books
	BEGIN
		INSERT INTO books_fts(books_fts, rowid, title, author, description, series)
		VALUES('delete', old.id, old.title, old.author, old.description, old.series);
	END;
`
const createUpdateTrigger = `
CREATE TRIGGER books_au AFTER UPDATE ON books
	BEGIN
		INSERT INTO books_fts(books_fts, rowid, title, author, description, series)
		VALUES('delete', old.id, old.title, old.author, old.description, old.series);
		INSERT INTO books_fts (rowid, uuid, title, author, description, series)
		VALUES (new.id, new.uuid, new.title, new.author, new.description, new.series);
	END;
`

// Store wraps a database connection to act as a database abstraction layer
type Store struct {
	*sql.DB
	writeLock sync.Mutex
}

// New creates a new DBStore
func New(db *sql.DB) *Store {
	return &Store{db, sync.Mutex{}}
}

// GetDB returns a database ready to use
// It handles schema creation if needed
func GetDB() (db *sql.DB, err error) {
	dbPath, err := getDBPath()

	if err != nil {
		return
	}

	db, err = sql.Open("sqlite", dbPath)
	if err != nil {
		return
	}

	if !isDBSetup(db) {
		err = prepareDB(db)
	}
	return
}

// Get DB path from environment variables
func getDBPath() (string, error) {
	return xdg.DataFile("bookshelf/index.sqlite")
}

// Prepare database schema
func prepareDB(db *sql.DB) error {
	creationStatements := [...]string{
		createBooksTable,
		createUniquePathIndex,
		createUniqueUUIDIndex,
		createSearchTable,
		createInsertTrigger,
		createDeleteTrigger,
		createUpdateTrigger,
	}
	for _, stmt := range creationStatements {
		_, err := db.Exec(stmt)
		if err != nil {
			return err
		}
	}
	return nil
}

func isDBSetup(db *sql.DB) bool {
	_, err := db.Exec("SELECT 1 FROM books")
	// Expected error: "no such table: books"
	return err == nil
}

// CollectionStore allow to query the collection as a whole
type CollectionStore interface {
	GetCollectionLastModification() (time.Time, error)
}

const getMaxMod = "SELECT mod_time FROM books ORDER BY mod_time DESC LIMIT 1"

// GetCollectionLastModification will return the most recent mod_time
func (store *Store) GetCollectionLastModification() (time.Time, error) {
	t := time.Now()
	row := store.QueryRow(getMaxMod)
	err := row.Scan(&t)
	if err != nil {
		return t, err
	}
	return t, err
}
