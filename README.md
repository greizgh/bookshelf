# Bookshelf

Bookshelf is a lightweight epub online catalog.

It exposes your ebook collection as a webview and [OPDS catalog](https://specs.opds.io/opds-1.2).

## How to install

Assuming you have [go](https://golang.org/) installed, simply run:

```
go install gitlab.com/greizgh/bookshelf@latest
```

The binary should be available in `$(go env GOPATH)/bin`

## How to compile

    go build .

## How to use

Assuming your epub collection is in `/var/lib/ebooks`:

### 1. Index your collection

```
bookshelf index /var/lib/ebooks
```

The index database and media (covers and thumbnails) are stored in [XDG](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html) compliant directory.
This is usually `.local/share/bookshelf` on linux.
You can alter that behavior by setting the `XDG_DATA_HOME` environment variable:

```
XDG_DATA_HOME=/var/lib/other bookshelf index /var/lib/ebooks
```

### 2. Serve the index

```
bookshelf serve -b 127.0.0.1 -p 8080
```

This will serve the web UI on http://127.0.0.1:8080

The root OPDS feed is available at http://127.0.0.1:8080/opds

You can also use a search feed, for instance with [koreader](http://koreader.rocks/),
you can set the search OPDS URL: `http://<host:port>/opds/search.xml?term=%s`

## Non-goals

- TLS handling: rely on your favorite reverse proxy
- Authentication
- Collection management: use a tool like [calibre](https://calibre-ebook.com)

Also noteworthy:
- currently only epub format is supported

## License

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

[Full license](./COPYING)
