// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

package cover

import (
	"bytes"
	"image"
	"image/png"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPathGeneration(t *testing.T) {
	id := "ee88cff0-b4dc-4c06-9e45-8a2d71db7eef"

	assert.Equal(t, "bookshelf/covers/ee8/ee88cff0-b4dc-4c06-9e45-8a2d71db7eef.png", getDataCoverPath(id))
	assert.Equal(t, "bookshelf/thumbnails/ee8/ee88cff0-b4dc-4c06-9e45-8a2d71db7eef.png", getDataThumbnailPath(id))
}

func TestImageStorage(t *testing.T) {
	id := "ee88cff0-b4dc-4c06-9e45-8a2d71db7eef"

	tempDir, err := os.MkdirTemp("", "bookshelf-")
	require.NoError(t, err)
	defer os.RemoveAll(tempDir)

	store := New(tempDir)

	img := image.NewRGBA(image.Rect(0, 0, 100, 100))

	err = store.SaveCover(id, img)
	require.NoError(t, err)

	var fileBuf, imgBuf bytes.Buffer

	_, err = store.GetCover(id, &fileBuf)
	require.NoError(t, err)

	err = png.Encode(&imgBuf, img)
	require.NoError(t, err)

	assert.Equal(t, imgBuf, fileBuf)
}

func TestThumbnailing(t *testing.T) {
	id := "ee88cff0-b4dc-4c06-9e45-8a2d71db7eef"

	tempDir, err := os.MkdirTemp("", "bookshelf-")
	require.NoError(t, err)
	defer os.RemoveAll(tempDir)

	store := New(tempDir)

	img := image.NewRGBA(image.Rect(0, 0, 1000, 1000))

	err = store.SaveCover(id, img)
	require.NoError(t, err)

	var buf bytes.Buffer

	_, err = store.GetThumbnail(id, &buf)
	require.NoError(t, err)

	thumb, err := png.Decode(&buf)
	require.NoError(t, err)

	assert.Equal(t, thumb.Bounds().Dy(), thumbnailHeight)
}
