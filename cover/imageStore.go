// Copyright 2020, 2021 Greizgh
//
// This file is part of bookshelf.
//
// bookshelf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bookshelf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with bookshelf.  If not, see <https://www.gnu.org/licenses/>.

// Package cover handles book cover storage
// All covers are stored as PNG, with names derived from book ID
package cover

import (
	"image"
	"image/png"
	"io"
	"os"
	"path"

	"github.com/nfnt/resize"
)

const thumbnailHeight = 200

// ImageStore is a common interface to manipulate book covers.
// It covers (pun intended) saving/retrieving images and thumbnails
type ImageStore interface {
	SaveCover(id string, img image.Image) error
	GetCover(id string, w io.Writer) (written int64, err error)
	GetThumbnail(id string, w io.Writer) (written int64, err error)
}

// FSStore is an ImageStore which stores images on the filesystem
type FSStore struct {
	dataDir string
}

// New FSstore with dataDir as root directory
func New(dataDir string) *FSStore {
	return &FSStore{dataDir: dataDir}
}

// SaveCover stores both book cover and thumbnail as PNG in data directory
func (s *FSStore) SaveCover(id string, img image.Image) error {
	imagePath := path.Join(s.dataDir, getDataCoverPath(id))
	thumbnailPath := path.Join(s.dataDir, getDataThumbnailPath(id))

	for _, p := range []string{imagePath, thumbnailPath} {
		dir := path.Dir(p)
		err := os.MkdirAll(dir, 0750)

		if err != nil {
			return err
		}
	}

	cover, err := os.Create(imagePath)
	if err != nil {
		return err
	}
	defer cover.Close()

	err = png.Encode(cover, img)
	if err != nil {
		return err
	}

	thumbnail, err := os.Create(thumbnailPath)
	if err != nil {
		return err
	}
	defer thumbnail.Close()

	// Fix height while preserving aspect ratio
	t := resize.Resize(0, thumbnailHeight, img, resize.Bilinear)
	err = png.Encode(thumbnail, t)

	return err
}

// GetCover retrieves the cover for book with given ID
func (s *FSStore) GetCover(id string, w io.Writer) (written int64, err error) {
	imagePath := path.Join(s.dataDir, getDataCoverPath(id))
	return getImage(imagePath, w)
}

// GetThumbnail retrieves the thumbnail for book with given ID
func (s *FSStore) GetThumbnail(id string, w io.Writer) (written int64, err error) {
	thumbPath := path.Join(s.dataDir, getDataThumbnailPath(id))
	return getImage(thumbPath, w)
}

func getImage(path string, w io.Writer) (written int64, err error) {
	file, err := os.Open(path)
	if err != nil {
		return 0, err
	}
	defer file.Close()

	return io.Copy(w, file)
}

// Get cover path relative to data directory
func getDataCoverPath(id string) string {
	return path.Join("bookshelf/covers", id[:3], id+".png")
}

// Get thumbnail path relative to data directory
func getDataThumbnailPath(id string) string {
	return path.Join("bookshelf/thumbnails", id[:3], id+".png")
}
